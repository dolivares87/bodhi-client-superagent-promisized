import test from 'ava';
import bodhiClient from '../src';
import createNormalBodhiClient from './fixtures/createNormalBodhiClient';
import getPath from 'lodash.get';

const transformedMethods = [
  'postAll',
  'post',
  'put',
  'patch',
  'delete',
  'get',
  'getPages',
  'getAll',
  'fetch',
  'serverUpsert',
  'upsert',
  'fetchAndUpdate',
  'fetchAndExtend',
  'fetchAndPatch',
  'fetchAndRemove',
  'findAndUpdate',
  'findAndRemove',
  'gather',
  'gatherAll',
  'enqueue',
  'send',
  'batch',
  'file.upload',
  'file.uploadFile',
  'file.uploadContent',
  'file.download',
  'file.createDownloadStream',
  'file.downloadBinary',
  'bulk.post',
  'bulk.insert',
  'bulk.update',
  'bulk.upsert',
  'bulk.invite'
];

function isThenable(suspectInQuestion) {
  return suspectInQuestion.then && typeof suspectInQuestion.then === 'function';
}

test('Create new bodhiClient instance but with promises', t => {
  const client = bodhiClient({
    uri: 'https://api.fake.io',
    namespace: 'fake',
    credentials: {
      userName: 'fakeUsername',
      passWord: 'fakePassword'
    },
    timeout: 60000
  });

  transformedMethods.every(method => {
    const methodTested = getPath(client, method);

    t.true(isThenable(methodTested()), `Should return a promise for method ${method}`);
  });
});

test('Modify existing bodhiClient methods', t => {
  const client = createNormalBodhiClient({
    uri: 'https://api.fake.io',
    namespace: 'fake',
    credentials: {
      userName: 'fakeUsername',
      passWord: 'fakePassword'
    },
    timeout: 60000
  });

  const promisizedClient = bodhiClient.promisizeClient(client);

  transformedMethods.every(method => {
    const methodTested = getPath(promisizedClient, method);

    t.true(isThenable(methodTested()), `Should return a promise for method ${method} on real client`);
  });
});
