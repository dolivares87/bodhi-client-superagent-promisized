import bodhiSuperAgent from 'bodhi-driver-superagent';

const Client = bodhiSuperAgent.Client;
const Basic = bodhiSuperAgent.BasicCredential;

export default function createBodhiClient({ uri, namespace, credentials }) {
  const { userName, passWord } = credentials;

  const client = new Client({
    uri,
    namespace,
    credentials: new Basic(userName, passWord),
    timeout: 60000
  });

  return client;
}
