export default function promisify(originalFn) {
  return (...args) => {
    return new Promise((resolve, reject) => {
      function callback(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve(data);
      }

      originalFn(...[...args, callback]);
    });
  };
}
