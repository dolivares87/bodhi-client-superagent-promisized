import promisify from './lib/promisify';
import getPath from 'lodash.get';
import setPath from 'lodash.set';

const methods = [
  'postAll',
  'post',
  'put',
  'patch',
  'delete',
  'get',
  'getPages',
  'getAll',
  'fetch',
  'serverUpsert',
  'upsert',
  'fetchAndUpdate',
  'fetchAndExtend',
  'fetchAndPatch',
  'fetchAndRemove',
  'findAndUpdate',
  'findAndRemove',
  'gather',
  'gatherAll',
  'enqueue',
  'send',
  'batch',
  'file.upload',
  'file.uploadFile',
  'file.uploadContent',
  'file.download',
  'file.createDownloadStream',
  'file.downloadBinary',
  'bulk.post',
  'bulk.insert',
  'bulk.update',
  'bulk.upsert',
  'bulk.invite'
];

export default function promisizeMethods(client) {
  methods.forEach(method => {
    const promisifiedFn = promisify(getPath(client, method));

    setPath(client, method, promisifiedFn);
  });

  return client;
}
