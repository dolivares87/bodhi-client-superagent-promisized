import 'source-map-support/register';
import promisifyMethods from './promisifyMethods';
import BodhiDriver from 'bodhi-driver-superagent';

const Client = BodhiDriver.Client;
const Basic = BodhiDriver.BasicCredential;

function bodhiClient(opts = {}) {
  const { credentials: { userName, passWord } } = opts;

  const clientOpts = Object.assign({}, opts, {
    credentials: new Basic(userName, passWord)
  });

  const client = promisifyMethods(new Client(clientOpts));

  return client;
}

bodhiClient.promisizeClient = function promisizeClient(client) {
  return promisifyMethods(client);
};

export default bodhiClient;
